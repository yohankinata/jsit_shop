<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;
use App\Category;
use App\SubCategory;
use App\Config;

class PageController extends FrontEndController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $categories = Category::where('deleted',0)->limit(3)->orderBy('id','asc')->get();
        return view('frontend.home',[
            'categories' => $categories
        ]);
    }

    public function contact()
    {
      return view('frontend.contact');
    }  

        //FIRST CONFIG//

        /*$data1['title'] = 'Women Collection 2018';
        $data1['description'] = 'NEW SEASON';
        $data1['image'] = 'config/slider/slide-01.jpg';
        $data1['link'] = url('product');
        $data1['linkText'] = 'Shop Now';

        $data2['title'] = 'Men New-Season';
        $data2['description'] = 'Jackets & Coats';
        $data2['image'] = 'config/slider/slide-02.jpg';
        $data2['link'] = url('blog');
        $data2['linkText'] = 'Purchase Now';

        $data3['title'] = 'Men Collection 2018';
        $data3['description'] = 'New arrivals';
        $data3['image'] = 'config/slider/slide-03.jpg';
        $data3['link'] = url('contact');
        $data3['linkText'] = 'Read Now';

        $sliders=[$data1,$data2,$data3];

        $config = Config::find(1);
        $config->title = 'JSIT Commerce';
        $config->tagline = 'Free shipping for standard order over Rp 1.000.000';
        $config->logo = 'config/logo-01.png';
        $config->icon = 'config/favicon.png';
        $config->email = 'info@jsit.com';
        $config->phone = '+1234567';
        $config->lat = '-6.385589';
        $config->lng = '106.830711';
        $config->address = 'Jl.Depok Raya No.35 Kota Depok Jawa Barat';
        $config->about = 'x';
        $config->faq = 'x';
        $config->slider = json_encode($sliders);
        $config->facebook = 'http://www.facebook.com';
        $config->instagram = 'http://www.instagram.com';
        $config->whatsapp = '6289680667362';
        echo $config->save()?'yes':'no';*/
}
