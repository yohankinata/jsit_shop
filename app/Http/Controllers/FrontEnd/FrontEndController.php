<?php

namespace App\Http\Controllers\FrontEnd;
use App\Http\Controllers\Controller;

use View;
use App\Config;
use App\SubCategory;
use App\Product;

class FrontEndController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $CONF = Config::find(1);
        $lastCat = SubCategory::selectRaw('name,productcategory.categoryId,count(productcategory.id) as totalProduct')
        		->join('productcategory','productcategory.categoryId','subcategories.id')
        		->where('deleted',0)
        		->orderBy('subcategories.id','desc')
        		->groupBy('productcategory.categoryId','name','subcategories.id')
        		->limit(4)
        		->get();
        $randProducts = Product::select('id','images')
        				->where('deleted',0)
        				->limit(9)
        				->inRandomOrder()
        				->get();
        View::share('CONF',$CONF);
        View::share('lastCategories',$lastCat);
        View::share('randProducts',$randProducts);
    }
}
