<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public $timestamps = false;
    protected $table = 'subcategories';

    public function product()
    {
    	return $this->belongsToMany(Product::class,'productcategory','productId','categoryId');
    }

    public function category()
    {
    	return $this->belongsTo(Category::class,'categoryId');
    }

    /*public function productCount() {
	    return $this->belongsToMany(Product::class,'productcategory','productId','categoryId')
	        ->selectRaw('count(productcategory.productId) as totalProduct')
	        ->groupBy('productcategory.categoryId');
	}

	public function getProductCountAttribute()
    {
        if ( ! array_key_exists('productCount', $this->relations)) $this->load('productCount');

        $related = $this->getRelation('productCount')->first();

        return ($related) ? $related->totalProduct : 0;
    }*/
}
