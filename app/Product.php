<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    
    public function category()
    {
    	return $this->belongsToMany(SubCategory::class,'productcategory','productId','categoryId');
    }

    public function province()
    {
    	return $this->hasOne(Province::class,'provinceId');
    }

    public function city()
    {
    	return $this->hasOne(City::class,'cityId');
    }
}
