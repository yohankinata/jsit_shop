<?php

use Illuminate\Database\Seeder;
use App\City;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = Storage::files('public/products');
        $photos = array();
        foreach ($files as $file) {
            array_push($photos, str_replace('public/', '', $file));
        }

        $countDocs = count($photos)-1;

        $faker = Faker\Factory::create();

        $cities = array();
        foreach (City::all() as $city) {
            $cit['city'] = $city->id;
            $cit['province'] = $city->provinceId;
            array_push($cities, $cit);
        }

        $material = ['cotton','metal','wood','nilon','gold','silver','cloth','wool'];
        $color = ['white','blue','red','black','green','yellow'];
        $size = ['S','M','L','XL','XXL'];

        $products = array();

        for ($i=0; $i < 100; $i++) { 
        	$data['code'] = 'PD-'.$i;
            $data['name'] = $faker->sentence($nbWords = 3, $variableNbWords = true);
            $data['description'] = implode('<br><br>', $faker->paragraphs($nb = 4, $asText = false));
            $cit_index = mt_rand(0,(count($cities)-1));
            $data['provinceId'] = $cities[$cit_index]['province'];
            $data['cityId'] = $cities[$cit_index]['city'];
            $data['stock'] = mt_rand(5,100);
            $data['price'] = $faker->randomNumber(5);
            $data['weight'] = $faker->randomFloat(2,0,2);
            $data['material'] = $material[mt_rand(0,(count($material)-1))];
            $col = array();
            for ($j=0; $j < (mt_rand(1,(count($color)-1))); $j++) { 
            	array_push($col, $color[mt_rand(0,(mt_rand(1,(count($color)-1))))]);
            }
            $data['color'] = implode(',', $col);

            $siz = array();
            for ($k=0; $k < (mt_rand(1,(count($size)-1))); $k++) { 
            	array_push($siz, $size[(mt_rand(1,(count($size)-1)))]);
            }
            $data['size'] = implode(',', $siz);

            $data['note'] = $faker->paragraph($nbSentences = 12, $variableNbSentences = true);
            $data['label'] = mt_rand(0,3);
            $image = array();
            for ($l=0; $l < (mt_rand(0,$countDocs)); $l++) {
            	array_push($image, $photos[mt_rand(0,$countDocs)]);
            }
            $data['images'] = json_encode($image);
            $data['created_at'] = $faker->dateTime();
            $data['updated_at'] = $faker->dateTime();

            array_push($products, $data);
        }

        DB::table('products')->insert($products);
    }
}
