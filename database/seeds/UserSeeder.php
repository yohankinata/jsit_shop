<?php

use Illuminate\Database\Seeder;
use App\City;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'kincat only',
            'username' => 'kincat',
            'email' => 'kincat@gmail.com',
            'phone' => '123456',
            'password' => bcrypt('1234'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role'	=> 'ADMIN'
        ]);

        DB::table('users')->insert([
            'name' => 'kincat only',
            'username' => 'kincat1',
            'email' => 'kincat1@gmail.com',
            'phone' => '123456',
            'password' => bcrypt('1234'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role'  => 'ADMIN'
        ]);

        $faker = Faker\Factory::create();

        $files = Storage::files('public/users');
        $photos = array();
        foreach ($files as $file) {
            array_push($photos, str_replace('public/', '', $file));
        }

        $countDocs = count($photos)-1;

        $cities = array();
        foreach (City::all() as $city) {
            $cit['city'] = $city->id;
            $cit['province'] = $city->provinceId;
            array_push($cities, $cit);
        }

        $users = array();

        for ($i=0; $i < 50; $i++) { 
            $data['name'] = $faker->name;
            $data['username'] = $faker->email;
            $data['email'] = $faker->email;
            $data['phone'] = $faker->e164PhoneNumber;
            $data['password'] = bcrypt('1234');
            $data['role'] = 'USER';
            $data['created_at'] = $faker->dateTime();
            $data['updated_at'] = $faker->dateTime();
            $data['updated'] = mt_rand(0,1);
            $data['photo'] = $photos[mt_rand(0,$countDocs)];
            $data['registerType'] = mt_rand(0,1);

            //set city and province
            $cit_index = mt_rand(0,(count($cities)-1));
            $data['provinceId'] = $cities[$cit_index]['province'];
            $data['cityId'] = $cities[$cit_index]['city'];
            $data['district'] = $faker->sentence($nbWords = 2, $variableNbWords = true);
            $data['address'] = $faker->paragraph($nb = 5, $asText = true);
            $data['postcode'] = mt_rand(1000,9999);

            array_push($users, $data);
        }

        DB::table('users')->insert($users);


    }
}
