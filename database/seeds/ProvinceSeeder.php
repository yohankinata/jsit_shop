<?php

use Illuminate\Database\Seeder;
use \RajaOngkir;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert([
            'id' => 0,
            'name' => 'Tidak Ada'
        ]);

        DB::table('cities')->insert([
            'id' => 0,
            'provinceId' => 0,
            'name' => 'Tidak Ada'
        ]);

        // //set province from raja ongkir
        // $provinces = RajaOngkir::Provinsi()->all();
        // $data_provinces = array();
        // foreach ($provinces as $province) {
        //     $data['id'] = $province['province_id'];
        //     $data['name'] = $province['province'];
        //     array_push($data_provinces, $data);
        // }

        // $insert = Province::insert($data_provinces);

        // //set city from raja ongkir
        // $cities = RajaOngkir::Kota()->all();
        // $data_city = array();
        // foreach ($cities as $city) {
        //     $data['id'] = $city['city_id'];
        //     $data['name'] = $city['type'].' '.$city['city_name'];
        //     $data['provinceId'] = $city['province_id'];
        //     array_push($data_city, $data);
        // }

        // $insert = City::insert($data_city);
    }
}
