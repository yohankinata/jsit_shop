<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', 'FrontEnd\PageController@index')->name('home');
Route::get('/about', 'FrontEnd\PageController@about')->name('about');
Route::get('/help', 'FrontEnd\PageController@help')->name('help');
Route::get('/contact', 'FrontEnd\PageController@contact')->name('contact');
Route::post('/contact', 'FrontEnd\PageController@contactSend')->name('contactSend');

Route::get('/product', 'FrontEnd\PageController@product')->name('product');
Route::get('/cart', 'FrontEnd\PageController@cart')->name('cart');
Route::get('/checkout', 'FrontEnd\PageController@checkout')->name('checkout');
Route::get('/confirm', 'FrontEnd\PageController@confirm')->name('confirm');

Route::get('/login', 'FrontEnd\PageController@login')->name('login');

// Route::get('/setProvince', 'HomeController@setProvince')->name('setProvince');
// Route::get('/setCity', 'HomeController@setCity')->name('setCity');


//Route::get('/','Auth\LoginController@showLoginForm')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
